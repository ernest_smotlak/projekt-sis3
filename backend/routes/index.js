var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var cors = require('cors')
var router = express.Router();

var con = mysql.createConnection({
  host: "remotemysql.com",
  user: "SqNjRj7nLx",
  password: "1OyJfVpqS2",
  database: "SqNjRj7nLx",
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

router.use(bodyParser.json());
router.use(cors());

async function doQuery(query) {
  return new Promise((resolve, reject) => {
    con.query(query, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    })
  })
}

router.post('/login', async (request, response) => {
  const {
    username,
    password
  } = request.body;

  if (username === undefined || password === undefined) {
    response.statusCode = 400;
    response.send();

    return;
  }

  const user = await doQuery(`SELECT id, password FROM users WHERE username='${username}'`);

  if (user.length > 0) {
    if (user[0].password !== password) {
      response.statusCode = 400;
      response.send();

      return;
    }

    response.json({
      userId: user[0].id,
    });
  } else {
    await doQuery(`INSERT INTO users (username, password) VALUES ('${username}', '${password}')`);
  
    const createdUser = await doQuery(`SELECT id FROM users WHERE username='${username}' AND password='${password}'`);

    response.json({
      userId: createdUser[0].id,
    });
  }

  response.statusCode = 200;
  response.send();
});

router.post('/court', async (request, response) => {
  const {
    courtName,
    courtLocation,
    description
  } = request.body;

  if (courtName === undefined || courtLocation === undefined || description === undefined) {
    response.statusCode = 400;
    response.send();

    return;
  }

  await doQuery(`INSERT INTO courts (court_name, court_location, description) VALUES ('${courtName}', '${courtLocation}', '${description}')`)

  response.statusCode = 201;
  response.send();
});

router.get('/courts', async (_request, response) => {
  const courts = await doQuery('SELECT * FROM courts');

  response.statusCode = 200;
  response.json(courts);
});

router.post('/game', async (request, response) => {
  const {
    gameName,
    gameDescription,
    courtId,
  } = request.body;

  if (gameName === undefined || gameDescription === undefined || courtId === undefined) {
    response.statusCode = 400;
    response.send();

    return;
  }

  await doQuery(`INSERT INTO games (game_name, game_description, court_id) VALUES ('${gameName}', '${gameDescription}', '${courtId}')`); 

  response.statusCode = 201;
  response.send();
});

router.get('/game/:id', async (request, response) => {
  const gameId = request.params.id;

  const game = await doQuery(`SELECT * FROM games WHERE id=${gameId}`);
  const attendes = await doQuery(`SELECT * FROM attendes WHERE game_id='${game[0].id}'`);

  response.statusCode = 201;
  response.json({
    ...game[0],
    attendes,
  });
});

router.get('/games/:id', async (request, response) => {
  const courtId = request.params.id;

  const games = await doQuery(`SELECT * FROM games WHERE court_id=${courtId}`)

  response.statusCode = 201;
  response.json(games);
});

router.post('/join-game', async (request, response) => {
  const {
    gameId,
    userId,
  } = request.body;

  if (gameId === undefined || userId === undefined) {
    response.statusCode = 400;
    response.send();

    return;
  }

  await doQuery(`INSERT INTO attendes (game_id, user_id) VALUES ('${gameId}', '${userId}')`);

  response.statusCode = 200;
  response.send();
});

module.exports = router;
